//alert('Hello world!')

/*Selection Control Structures
	- sorts out whether the statement/s are to be executed based on the condition whether it is true or false

	if .. else statement

	Syntax:
		if(condition) {
			//statement
		} else {
			//statement
		}

	if statement - Executes a statement if a specified conditon is true

	Syntax:
		if(condition) {
			//statement
		}

*/

let numA = -1;

if(numA < 0) {
	console.log("Hello");
}

console.log(numA < 0)


let city = "New York"

if (city === "New York") {
	console.log("Welcome to New York City");
}

/*
	Else if 
		- executes a statement if previous conditions are false and if the specified condition is true
		-the "else if" clase us optional and can be added to capture additional condition to change the flow of a program
*/		

let numB = 1;

if (numA > 0) {
	console.log("Hello");
}	else if (numB > 0) {
	console.log("World")
	
}

//Another Example:
city = "Tokyo" ;

if(city === "New Work") {
	console.log("Welcome to New York City")
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo")
}


/*
	else statement
		-executes a statement if all other conditions are false
*/


if(numA > 0) {
	console.log("Hello")
} else if (numB === 0) {
	console.log("World")
} else {
	console.log("Again");
}


//parseInt - number only


/*let age = parseInt(prompt("Enter your age: "));

if(age <= 18) {
	console.log("Not allowed to drink!")
} else {
	console.log("Matanda ka na, shot na!");
}
*/


// Mini activity



let height = 160;

if (height < 150) {
	console.log("Did not passed the min height req")
} else {
	console.log("Passed the min height req")
}


function heightReq(height) {
	if (height < 150) {
	console.log("Did not passed the min height req")
} else {
	console.log("Passed the min height req")
}
}

heightReq(140)
heightReq(160)


let message = "No message";
console.log(message)

function determineTyphoonIntensity(windSpeed){
		if(windSpeed < 30) {
			return'Nota typhoon yet'
		} else if(windSpeed <= 61) {
			return 'Tropical depression detected'
		} else if (windSpeed >= 62 && windSpeed <= 88) {
			return 'Tropical storm detected.'
		} else if (windSpeed >= 89 && windSpeed<= 117) {
			return 'Severe tropical storm detected'
		} else {
			return 'Typhoon detected.'
		}


}

message = determineTyphoonIntensity(70)
console.log(message)

console.log(determineTyphoonIntensity(90))

if (message == 'Tropical storm detected.') {
	console.warn(message);
}


//Truthy and Falsy

/*
	In JS. a truthy value is a value that is considered true when encounted in a boolean context.

	Falsy Values
		1. false
		2. 0
		3. -0
		4.""
		5. null
		6. undefined
		7. NaN

*/

if (true) {
	console.log("Truthy")
}


if (1) {
	console.log("Truthy");
}

if ([]) {
	console.log("Truthy")
}


//Falsy Examples
if(false) {
	console.log("Falsy")
}


if(0) {
	console.log("Falsy")
}

if (undefined) {
	console.log("Falsy")
}


// Conditional (Ternary) Operator
/*
	Ternary Operator takes in three operands.
		1. condition
		2. expression to execute if the condition is truthy
		3. expression to execute if the condition is falsy

	Syntax:
		(condition) ? ifTrue_expression : ifFalse_expression
*/

//Single Statement Execution
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of the Ternary Operator: " + ternaryResult) // true

let name;

function isOfLegalAge() {
	name = 'John'
	return 'You are of the legal age limit';
}

function isUnderAge() {
	name = 'Jane';
	return 'You are under the age limit';
}

/*let age = parseInt(prompt("What is your age?"));
console.log(age)
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in Functions: " + legalAge + ', ' + name)*/

//Switch Statement
/*
	Can be used as an alternative to an if.. else statement where the data to be used in the condition is of an expected input

	Syntax:
		switch (expression) {
			case <value>:
				statement;
				break;

			default:
				statement;
				break;
		}
*/

let day = prompt("What day of the week is it today?").toLowerCase() 
console.log(day);

switch (day) {
	case 'monday':
		console.log("The color of the day is red");
		break;

	case 'tuesday':
		console.log("The color of the day is orange");
		break;

	case 'wednesday':
		console.log("The color of the day is yellow");
		break;
	case 'thursday':
		console.log("The color of the day is green");
		break;
	case 'friday':
		console.log("The color of the day is blue");
		break;
	case 'satuday':
		console.log("The color of the day is indigo");
		break;
	case 'sunday':
		console.log("The color of the day is violet");
		break;

	default:
		console.log("Please input a valid day");
		break;
}


//Try -Catch-Finally Statement
/*
	-try catch are commonly used for error handling
*/

function showIntensityAlert(windSpeed) {
	try {
		alerat(determineTyphoonIntensity(windSpeed))
	}

	catch (error){
		console.log(typeof error)
		console.warn(error.message)
	}

	finally {
		alert("Intensity updates will show new alert!!")
	}
}
showIntensityAlert(56)
